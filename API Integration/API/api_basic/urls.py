from django.urls import path
from .views import article_list,article_detail
from .api_views import article_list,article_detail
from .class_api_views import article_list,article_detail


urlpatterns = [
    #url(r'^admin/', admin.site.urls),
path('views/article/', article_list),
path('views/article_detail/<int:pk>',article_detail),
path('api_views/article/', article_list),
path('api_views/article_detail/<int:pk>',article_detail),
path('class_api_views/article/', article_list.as_view()),
path('class_api_views/article_detail/<int:id>/',article_detail.as_view())




]
