# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from .models import Article
from .serializer import ArticleSerializer
from .serializer import ArticleModelSerializer
from rest_framework.decorators import api_view
from rest_framework.response  import Response
from rest_framework import status
from rest_framework.views import APIView

# Create your views here.


@api_view(['GET','POST']) # api_view
#@csrf_#exempt Function views
def article_list(request):
    if request.method == "GET":
        articles = Article.objects.all()
        serializer = ArticleModelSerializer(articles, many = True)
        #return JsonResponse(serializer.data, safe = False) # Function Views
        return Response(serializer.data)
    elif request.method == "POST":
        data = JSONParser().parse(request)
        serializer = ArticleModelSerializer(data= data)
        if serializer.is_valid():
            serializer.save()
            #return JsonResponse(serializer.data,status=201) # Function Views
            return  Response(serializer.data,status = status.HTTP_201_CREATED)
        #return JsonResponse(serializer.errors,status=400)   # Function Views
        return Response(serializer.data, status = status.HTTP_400_BAD_REQUEST)

@api_view(['GET','PUT','DELETE']) # api_view
#@csrf_#exempt Function views
def article_detail(request, pk):
    try:
        article = Article.objects.get(pk=pk)

    except Article.DoesNotExist:
        #return HttpResponse(status=404) #Function Views
        return Response(status = status.HTTP_404_NOT_FOUND) #Function Views

    if request.method == "GET":
        serializer = ArticleModelSerializer(article)
        #return  JsonResponse(serializer.data) #Function Views
        return Response(serializer.data)
    elif request.method == "PUT":
        # data = JSONParser().parse(request) # Function Views
        serializer = ArticleModelSerializer(article,data=request.data)
        if serializer.is_valid():
           serializer.save()
           return Response(serializer.data)
        #return JsonResponse(serializer.errors, status =400) #Function Views
        return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)
    elif request.method == "DELETE":
           article.delete()
           #return  HttpResponse(status=204) #Function Views
           return Response(status = status.HTTP_204_NO_CONTENT)



