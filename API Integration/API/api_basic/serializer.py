from rest_framework import serializers
from .models import Article
from .models import Contact



class ArticleSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100,help_text="Enter Product Stock Keeping Unit")
    author = serializers.CharField(max_length=100,help_text="Enter Product Stock Keeping Unit")
    email = serializers.CharField(max_length=100,help_text="Enter Product Stock Keeping Unit")
    date = serializers.DateTimeField()


def create(self, validated_data):
    return Article.objects.create(validated_data)

def update(self, instance, validated_data):
    instance.title = validated_data.get('title',instance.title)
    instance.author = validated_data.get('author', instance.author)
    instance.email = validated_data.get('email', instance.email)
    instance.date = validated_data.get('date', instance.date)
    instance.save()
    return instance

class ContactSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100,help_text="Enter Product Stock Keeping Unit")
    mobile = serializers.CharField(max_length=100,help_text="Enter Product Stock Keeping Unit")
    email = serializers.CharField(max_length=100,help_text="Enter Product Stock Keeping Unit")
    date = serializers.DateTimeField()


def create(self, validated_data):
    return Contact.objects.create(validated_data)

def update(self, instance, validated_data):
    instance.name = validated_data.get('name',instance.name)
    instance.mobile = validated_data.get('mobile', instance.mobile)
    instance.email = validated_data.get('email', instance.email)
    instance.date = validated_data.get('date', instance.date)
    instance.save()
    return instance

class ArticleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model= Article
        fields = ['id','title','author']
        #fields = '_all_'